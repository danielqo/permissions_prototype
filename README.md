# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version: 2.6.3

* System dependencies: MySQL

## Steps to start from scratch:
- `rails g bootstrap:install static --no-coffeescript`
- `rails g bootstrap:layout`
- `rails g rspec:install`
- `rails g devise:install`
- Add `//= link application.js` to `app/assets/config/manifest.js`

### Database creation
- `rails db:create db:migrate db:seed`

### Configuring RSpec

spec_helper.rb
```
  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
```

rails_helper.rb
```
Shoulda::Matchers.configure do |config|
    config.integrate do |with|
        with.test_framework :rspec
        with.library :rails
    end
end
```

### Create User model using Devise
`rails g devise User`

`rails db:migrate`

### Create Blog Post scaffold
`rails g scaffold Post title:string body:text user:references`

`rails db:migrate`

Modify PostsController:
  `before_action :authenticate_user!`

### Create Blog Comment scaffold
`rails g model Comment body:text user:references post:references`

`rails db:migrate`

### Create Role model
`rails g model Role name:string`

 Add some validation:
 `validates :name, presence: true, uniqueness: true`

 `rails db:migrate`

### Create Role Assignment model
`rails g model Assignment user:references role:references`

Modify User model

`has_many :assignments`

`has_many :roles, through: :assignments`

### Defining CanCancan abilities

First, see https://medium.com/@coorasse/cancancan-that-scales-d4e526fced3d

Add to `application.rb`: `config.autoload_paths << Rails.root.join('app/abilities')`

Create `app/abilities/post_ability.rb`

Create `app/abilities/comment_ability.rb`

Edit `PostsController`:
- add `authorize! :xxx, yyy` to all methods (do not use `load_and_authorize_resource`)
- define which Ability to use to authorize actions in `current_ability` method

Similarly to `CommentsController`.

At this point, you have:
- a Blog with Posts and Comments
- Users with different abilities:
  - ability to create Posts (editor role)
    - each Post can be edited/removed by its author
  - ability to create Comments (commenter role)
    - each Comment can be edited/removed by its author
    - each Comment can be edited/removed by the Post's author
  - ability to read everything (no role)
- All application options (e.g. New Post, Edit Post, Destroy Post, etc) are visible to all users, regardless their roles.

### Implementing "per resource basis" access:

As multiple table inheritance may be problematic to introduce in Allegory, then I thought it would be easier to control it "manually" in the Assignment model.

Create a migration to add columns to Assignment model:

`rails g migration AddResourceIDAndTypeToAssignment` 

Add columns `resource_id` and `resource_type`. In the context of this example application, a `resource` may be both a Post or a Comment. In Allegory context, it may be anything which access should be subject to authorization.

See `seeds.rb` to understand how I am creating a Post which can be edited by mmultiple users (author + another editor).

### Sources:

- https://github.com/heartcombo/devise 
- https://github.com/CanCanCommunity/cancancan
- https://github.com/CanCanCommunity/cancancan/wiki/defining-abilities 
- https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities-with-Blocks
- https://medium.com/@coorasse/cancancan-that-scales-d4e526fced3d
- https://github.com/CanCanCommunity/cancancan/wiki/Authorizing-Controller-Actions 




