class AddResourceIdAndTypeToAssignment < ActiveRecord::Migration[6.0]
  def change
    change_table :assignments do |t|
      t.integer :resource_id
      t.string  :resource_type
    end
  end
end
