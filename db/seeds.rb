# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


admin = User.create!(email:'super.user@email.com', password:'secret1234', password_confirmation:'secret1234')
editor = User.create!(email:'editor.user@email.com', password:'secret1234', password_confirmation:'secret1234')
editor2 = User.create!(email:'editor.user2@email.com', password:'secret1234', password_confirmation:'secret1234')
commenter = User.create!(email:'nice.commenter@email.com', password:'secret1234', password_confirmation:'secret1234')
troll = User.create!(email:'troll.user@email.com', password:'secret1234', password_confirmation:'secret1234')

a = Role.create(name: 'admin')
e = Role.create(name: 'editor')
c = Role.create(name: 'commenter')

Assignment.create(user: admin, role: a)

Assignment.create(user: editor, role: e)
Assignment.create(user: editor, role: c)

Assignment.create(user: editor2, role: e)
Assignment.create(user: editor2, role: c)

Assignment.create(user: commenter, role: c)

(1..10).each do |i|
    Post.create(title: Faker::TvShows::DrWho.catch_phrase, body: Faker::Lorem.paragraphs.join, user: editor)
end

(1..10).each do |i|
    Post.create(title: Faker::TvShows::DrWho.catch_phrase, body: Faker::Lorem.paragraphs.join, user: editor2)
end
shared_post = Post.create(title: 'This is a shared post', body: 'Multiple editors can edit it', user: editor)

Assignment.create(user: editor2, role: e, resource_id: shared_post.id, resource_type: shared_post.class)