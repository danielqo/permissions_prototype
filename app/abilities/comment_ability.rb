class CommentAbility
    include CanCan::Ability

    def initialize(user)
      can :read, Comment

      if user.role?(:commenter)
        can :create, Comment
        can :manage, Comment, user_id: user.id
      end

      if user.role?(:editor)
        can :manage, Comment, post: {user_id: user.id}
      end

      if user.role?(:admin)
        can :manage, Comment
      end

    end
end