class PostAbility
    include CanCan::Ability

    def initialize(user)
      can :read, Post

      if user.role?(:editor)
        can :create, Post
        can :manage, Post, user_id: user.id
        can :manage, Post do |post|
            Assignment.where(resource_type: Post.name)
                      .where(resource_id: post.id)
                      .where(user_id: user.id)
                      .where(role_id: Role.find_by_name('editor').id).first
        end
      end

      if user.role?(:admin)
        can :manage, Post
      end

    end

end
